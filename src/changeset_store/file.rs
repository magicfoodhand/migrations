use crate::{Changeset, ChangesetStore, Identifier};
use std::fs::read_dir;
use std::path::PathBuf;

pub(crate) struct FileChangesetStore {
    pub path: PathBuf,
    pub empty_changeset: fn(Identifier) -> Box<dyn Changeset>,
    pub to_changeset: fn(PathBuf) -> Box<dyn Changeset>,
}

impl FileChangesetStore {
    pub(crate) fn create(
        path: PathBuf,
        empty_changeset: fn(Identifier) -> Box<dyn Changeset>,
        to_changeset: fn(PathBuf) -> Box<dyn Changeset>,
    ) -> Box<dyn ChangesetStore> {
        Box::new(FileChangesetStore {
            path,
            empty_changeset,
            to_changeset,
        })
    }
}

impl ChangesetStore for FileChangesetStore {
    fn create_changeset(&self, name: String) -> anyhow::Result<Box<dyn Changeset>> {
        let identifier = Identifier::now(&name);
        Ok((self.empty_changeset)(identifier))
    }

    fn get_changesets(&self) -> anyhow::Result<Vec<Box<dyn Changeset>>> {
        read_dir(self.path.as_path())?
            .filter(|entry| {
                let entry = entry.as_ref().unwrap();
                let binding = entry.file_name();
                let name = binding.to_str().unwrap();
                // patch matches regex, YYYYMMDDHHMMSS_<name, \w+>, 20240620112020_test
                let re = regex::Regex::new(r"\d{14}_\w+").unwrap();
                re.is_match(name)
            })
            .map(|entry| {
                let entry = entry?;
                Ok((self.to_changeset)(entry.path().to_path_buf()))
            })
            .collect()
    }

    fn clone(&self) -> Box<dyn ChangesetStore> {
        Box::new(FileChangesetStore {
            path: self.path.clone(),
            empty_changeset: self.empty_changeset,
            to_changeset: self.to_changeset,
        })
    }
}
