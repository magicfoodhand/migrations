use crate::{Identifier, MigrationStore};

#[cfg(feature = "files")]
mod file;

#[cfg(feature = "files")]
use file::FileChangesetStore;

#[cfg(feature = "sql")]
mod sql;

#[cfg(feature = "sql")]
pub use sql::{SqlChangeset, SqlChangesetStore};

pub struct ChangesetStores;

impl ChangesetStores {
    #[cfg(feature = "files")]
    pub fn file_store(
        path: std::path::PathBuf,
        empty_changeset: fn(Identifier) -> Box<dyn Changeset>,
        to_changeset: fn(std::path::PathBuf) -> Box<dyn Changeset>,
    ) -> Box<dyn ChangesetStore> {
        FileChangesetStore::create(path, empty_changeset, to_changeset)
    }

    #[cfg(feature = "sql")]
    pub fn sql_store(path: std::path::PathBuf) -> Box<dyn ChangesetStore> {
        SqlChangesetStore::new(path)
    }
}

pub trait ChangesetStore {
    fn create_changeset(&self, name: String) -> anyhow::Result<Box<dyn Changeset + '_>>;

    fn get_changesets(&self) -> anyhow::Result<Vec<Box<dyn Changeset>>>;

    fn clone(&self) -> Box<dyn ChangesetStore + '_>;
}

pub trait Changeset {
    fn identifier(&self) -> Identifier;

    fn apply(&self, store: &dyn MigrationStore) -> anyhow::Result<()>;

    fn rollback(&self, store: &dyn MigrationStore) -> anyhow::Result<()>;

    fn duplicate(&self) -> Box<dyn Changeset>;
}

impl Changeset for Box<dyn Changeset> {
    fn identifier(&self) -> Identifier {
        self.as_ref().identifier()
    }

    fn apply(&self, store: &dyn MigrationStore) -> anyhow::Result<()> {
        self.as_ref().apply(store)
    }

    fn rollback(&self, store: &dyn MigrationStore) -> anyhow::Result<()> {
        self.as_ref().rollback(store)
    }

    fn duplicate(&self) -> Box<dyn Changeset> {
        self.as_ref().duplicate()
    }
}
