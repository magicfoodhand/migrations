use crate::{Changeset, ChangesetStore, Identifier, MigrationStore};
use std::path::{Path, PathBuf};

pub struct SqlChangeset {
    pub identifier: Identifier,
    pub up: PathBuf,
    pub down: PathBuf,
}

impl SqlChangeset {
    pub fn create(current_directory: &Path, identifier: Identifier) -> Box<dyn Changeset> {
        let path = current_directory.join(identifier.value());
        Box::new(SqlChangeset {
            identifier,
            up: path.join("up.sql"),
            down: path.join("down.sql"),
        })
    }
}

impl Changeset for SqlChangeset {
    fn identifier(&self) -> Identifier {
        self.identifier.clone()
    }

    fn apply(&self, store: &dyn MigrationStore) -> anyhow::Result<()> {
        let actions = store.file_actions();
        if actions.is_none() {
            return Err(anyhow::anyhow!(
                "No file actions available for selected Migration Store"
            ));
        }
        let actions = actions.unwrap();
        actions.run(&self.up.as_path())
    }

    fn rollback(&self, store: &dyn MigrationStore) -> anyhow::Result<()> {
        let actions = store.file_actions();
        if actions.is_none() {
            return Err(anyhow::anyhow!(
                "No file actions available for selected Migration Store"
            ));
        }
        let actions = actions.unwrap();
        actions.run(&self.down.as_path())
    }

    fn duplicate(&self) -> Box<dyn Changeset> {
        Box::new(SqlChangeset {
            identifier: self.identifier.clone(),
            up: self.up.clone(),
            down: self.down.clone(),
        })
    }
}

pub struct SqlChangesetStore {
    pub path: PathBuf,
}

impl SqlChangesetStore {
    pub fn new(path: PathBuf) -> Box<dyn ChangesetStore> {
        Box::new(SqlChangesetStore { path })
    }
}

impl ChangesetStore for SqlChangesetStore {
    fn create_changeset(&self, name: String) -> anyhow::Result<Box<dyn Changeset + '_>> {
        let identifier = Identifier::now(&name);
        let path = self.path.join(identifier.value());
        std::fs::create_dir_all(&path)?;
        Ok(Box::new(SqlChangeset {
            identifier,
            up: path.join("up.sql"),
            down: path.join("down.sql"),
        }))
    }

    fn get_changesets(&self) -> anyhow::Result<Vec<Box<dyn Changeset>>> {
        Ok(self
            .path
            .read_dir()?
            .map(|entry| {
                let entry = entry.expect("Failed to read entry");
                SqlChangeset::create(
                    self.path.as_path(),
                    Identifier::from_string(entry.file_name().to_str().unwrap()),
                )
            })
            .collect())
    }

    fn clone(&self) -> Box<dyn ChangesetStore + '_> {
        Box::new(SqlChangesetStore {
            path: self.path.clone(),
        })
    }
}
