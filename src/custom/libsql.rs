use crate::migration_store::FileActions;
use crate::{Changeset, Identifier, MigrationStore};
use db_dsl::{
    Column, DataType, Definition, Dialect, PrimaryKey, Table, TableDefinition, TableName,
    TableOptions,
};
use libsql::{Connection, Rows};
use log::warn;
use std::borrow::ToOwned;
use std::fs;
use std::path::Path;
use std::string::ToString;

pub(crate) struct LibSqlMigrationStore {
    conn: Connection,
    setup_sql: String,
}

impl FileActions for LibSqlMigrationStore {
    fn run(&self, path: &Path) -> anyhow::Result<()> {
        fs::read_to_string(path)
            .map_err(|e| e.into())
            .and_then(|sql| {
                async_std::task::block_on(self.conn.execute(&sql, ()))
                    .map_err(|e| e.into())
                    .map(|_| ())
            })
    }
}

fn required_definition() -> Vec<Definition> {
    vec![
        Definition::table_no_timestamps(
            "changeloglock",
            vec![
                TableDefinition::column(
                    DataType::Bool
                        .column("locked")
                        .not_null()
                        .default(serde_value::Value::Bool(false))
                        .to_owned(),
                ),
                TableDefinition::column(DataType::Text.column("locked_by").not_null().to_owned()),
                TableDefinition::column(
                    DataType::Datetime
                        .column("locked_at")
                        .not_null()
                        .default_raw("CURRENT_TIMESTAMP".to_string())
                        .to_owned(),
                ),
            ],
        ),
        Definition::statement(
            "INSERT INTO changeloglock (id, locked, locked_by) VALUES (1, false, '')",
        ),
        Definition::table_no_timestamps(
            "migrations",
            vec![
                TableDefinition::column(DataType::Text.column("identifier").not_null().to_owned()),
                TableDefinition::column(
                    DataType::Datetime
                        .column("applied_at")
                        .not_null()
                        .default_raw("CURRENT_TIMESTAMP".to_string())
                        .to_owned(),
                ),
            ],
        ),
    ]
}

static DEFAULT_SQL: &str = r#"
CREATE TABLE IF NOT EXISTS changeloglock (
    id INTEGER PRIMARY KEY,
    locked BOOLEAN NOT NULL DEFAULT FALSE,
    locked_by TEXT NOT NULL,
    locked_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);

INSERT INTO changeloglock (id, locked, locked_by) VALUES (1, false, '');

CREATE TABLE IF NOT EXISTS migrations (
    id SERIAL PRIMARY KEY,
    identifier TEXT NOT NULL,
    applied_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);
"#;

use db_dsl::SqlUp;

impl LibSqlMigrationStore {
    pub(crate) fn new(conn: Connection) -> Box<dyn MigrationStore> {
        let setup_sql = required_definition()
            .iter()
            .sql_up(Dialect::Sqlite)
            .unwrap_or_else(|| {
                warn!("Failed to generate SQL with db_dsl, using default SQL instead");
                DEFAULT_SQL.to_string()
            });
        Box::new(Self {
            conn: conn.clone(),
            setup_sql,
        })
    }

    fn run_sql(&self, sql: &str) -> anyhow::Result<()> {
        async_std::task::block_on(self.conn.execute_batch(sql))?;
        Ok(())
    }

    fn returns_n(&self, rows: &mut Rows, n: usize) -> anyhow::Result<bool> {
        if n == 0 {
            return Ok(true);
        }

        let exists = async_std::task::block_on(rows.next())?.is_some();
        if !exists {
            warn!("Expected {} rows, got less", n);
            return Ok(false);
        }
        return Ok(exists && self.returns_n(rows, n - 1)?);
    }
}

impl MigrationStore for LibSqlMigrationStore {
    fn is_ready(&self) -> anyhow::Result<bool> {
        let mut rows = async_std::task::block_on(self.conn.query("SELECT name FROM sqlite_schema WHERE type='table' AND name='changeloglock' OR name='migrations';", ()))
            .map(|rows| rows )?;
        self.returns_n(&mut rows, 2)
    }

    fn setup(&self) -> anyhow::Result<()> {
        self.run_sql(&self.setup_sql)
    }

    fn is_applied(&self, identifier: &Identifier) -> bool {
        let rows = async_std::task::block_on(self.conn.query(
            "SELECT identifier FROM migrations WHERE identifier = $1;",
            [identifier.to_string()],
        ))
        .map(|rows| rows);

        if let Err(e) = rows {
            log::error!("Error checking if migration is applied: {:?}", e);
            return false;
        }

        let mut rows = rows.unwrap();

        self.returns_n(&mut rows, 1).unwrap_or_else(|e| {
            log::error!("Error checking if migration is applied: {:?}", e);
            false
        })
    }

    fn apply(&mut self, changeset: &dyn Changeset) -> anyhow::Result<()> {
        async_std::task::block_on(self.conn.execute(
            "INSERT INTO migrations (identifier) VALUES ($1);",
            [changeset.identifier().to_string()],
        ))
        .map(|_| ())
        .map_err(|e| e.into())
    }

    fn rollback(&mut self, changeset: &dyn Changeset) -> anyhow::Result<()> {
        async_std::task::block_on(self.conn.execute(
            "DELETE FROM migrations WHERE identifier = $1;",
            [changeset.identifier().to_string()],
        ))
        .map(|_| ())
        .map_err(|e| e.into())
    }

    fn clone(&self) -> Box<dyn MigrationStore> {
        Box::new(Self {
            conn: self.conn.clone(),
            setup_sql: self.setup_sql.clone(),
        })
    }

    fn file_actions(&self) -> Option<&dyn FileActions> {
        Some(self)
    }
}

#[cfg(test)]
mod tests {
    use crate::changeset_store::SqlChangeset;
    use crate::custom::libsql::LibSqlMigrationStore;
    use crate::Identifier;
    use libsql::Builder;
    use std::fs;

    #[test]
    fn libsql_migration_store() {
        pretty_env_logger::init();
        let mut db_identifier = Identifier::now("libsql_test").to_string();
        db_identifier.push_str(".db");
        let db =
            async_std::task::block_on(Builder::new_local(&db_identifier.as_str()).build()).unwrap();
        let conn = db.connect().unwrap();
        let current_directory = std::env::current_dir().unwrap();
        let path = current_directory.as_path();
        let mut store = LibSqlMigrationStore::new(conn);
        assert_eq!(store.is_ready().unwrap(), false);
        store.setup().unwrap();
        assert_eq!(store.is_ready().unwrap(), true);
        let identifier = Identifier::now("foo");
        store
            .apply(&SqlChangeset::create(&path, identifier.clone()))
            .unwrap();
        assert_eq!(store.is_applied(&identifier), true);

        store
            .rollback(&SqlChangeset::create(&path, identifier.clone()))
            .unwrap();
        assert_eq!(store.is_applied(&identifier), false);
        fs::remove_file(&db_identifier.as_str())
            .expect(format!("failed to cleanup test db file: {}", db_identifier.as_str()).as_str());
    }
}
