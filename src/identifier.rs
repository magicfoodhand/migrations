use chrono::{DateTime, Utc};
use std::fmt::Display;

#[derive(Clone, Debug, PartialEq, Hash, Eq)]
pub struct Identifier {
    value: String,
}

impl Identifier {
    pub fn new(time: DateTime<Utc>, name: &str) -> Self {
        let value = format!("{}_{}", time.format("%Y%m%d%H%M%S"), name);
        Self { value }
    }

    pub fn now(name: &str) -> Self {
        Self::new(Utc::now(), name)
    }

    pub fn from_string(value: &str) -> Self {
        Self {
            value: value.to_string(),
        }
    }

    pub fn value(&self) -> &str {
        self.value.as_str()
    }
}

impl From<&str> for Identifier {
    fn from(value: &str) -> Self {
        Identifier::from_string(value)
    }
}

impl From<String> for Identifier {
    fn from(value: String) -> Self {
        Identifier::from_string(value.as_str())
    }
}

impl Display for Identifier {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.value)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use chrono::NaiveDate;

    #[test]
    fn identifier_format() {
        let result = Identifier::new(
            NaiveDate::from_ymd_opt(2024, 1, 2)
                .unwrap()
                .and_hms_opt(3, 4, 5)
                .unwrap()
                .and_utc(),
            "foo",
        );
        assert_eq!(result.value(), "20240102030405_foo");
    }
}
