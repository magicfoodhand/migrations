use anyhow::Result;
use log::{error, info, warn};

pub use changeset_store::{Changeset, ChangesetStore, ChangesetStores};
pub use identifier::Identifier;
pub use migration_store::{FileActions, MigrationStore, MigrationStores};

#[cfg(feature = "sql")]
pub use changeset_store::{SqlChangeset, SqlChangesetStore};

#[cfg(feature = "sql")]
pub use custom::Dialect;

mod changeset_store;
mod custom;
mod identifier;
mod migration_store;

pub struct Migrations {
    store: Box<dyn MigrationStore>,
    changes: Box<dyn ChangesetStore>,
}

impl Migrations {
    pub fn new(store: Box<dyn MigrationStore>, changes: Box<dyn ChangesetStore>) -> Migrations {
        Migrations { store, changes }
    }

    pub fn setup(&self) -> Result<()> {
        if !self.store.is_ready()? {
            self.store.setup()?;
        }
        Ok(())
    }

    pub fn is_applied(&self, identifier: &Identifier) -> bool {
        self.store.is_applied(identifier)
    }

    pub fn create_changeset(&self, name: String) -> Result<Box<dyn Changeset + '_>> {
        self.changes.create_changeset(name)
    }

    pub fn reset(&mut self) -> Result<()> {
        for changeset in self.changes.get_changesets()? {
            if self.store.is_applied(&changeset.identifier()) {
                self.store.rollback(&changeset)?;
            }
        }
        Ok(())
    }

    pub fn rollback(&mut self) -> Result<()> {
        let changesets = self.changes.get_changesets()?;
        let last = changesets
            .iter()
            .filter(|c| self.store.is_applied(&c.identifier()))
            .max_by(|a, b| a.identifier().value().cmp(b.identifier().value()));

        if last.is_none() {
            warn!("No changeset_store to rollback");
            return Ok(());
        }
        let last = last.unwrap();
        match self.store.rollback(last) {
            Ok(_) => {
                info!("Rolled back {}", last.identifier());
                Ok(())
            }
            Err(e) => {
                error!("Failed to rollback {}", last.identifier());
                Err(e)
            }
        }
    }

    pub fn migrate(&mut self) -> Result<()> {
        for change in &self.changes.get_changesets()? {
            if !self.store.is_applied(&change.identifier()) {
                match self.store.apply(change) {
                    Ok(_) => {
                        info!("Applied {}", change.identifier())
                    }
                    Err(e) => {
                        error!("Failed to apply {}: {:?}", change.identifier(), e);
                        return Err(e);
                    }
                }
            }
        }
        Ok(())
    }

    #[cfg(feature = "libsql")]
    pub fn libsql(
        connection: libsql::Connection,
        migration_directory: std::path::PathBuf,
    ) -> Migrations {
        let store = MigrationStores::libsql(connection.clone());
        let changes = ChangesetStores::sql_store(migration_directory);
        Self::new(store, changes)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::changeset_store::ChangesetStores;
    use crate::migration_store::MigrationStores;
    use std::env;
    use std::fs::create_dir;
    use std::path::{Path, PathBuf};

    pub struct FileChangeset {
        path: PathBuf,
    }

    impl FileChangeset {
        pub fn new(path: PathBuf) -> Box<dyn Changeset> {
            Box::new(FileChangeset { path })
        }
    }

    impl Changeset for FileChangeset {
        fn identifier(&self) -> Identifier {
            Identifier::from_string(self.path.file_name().unwrap().to_str().unwrap())
        }

        fn apply(&self, _store: &dyn MigrationStore) -> anyhow::Result<()> {
            Ok(())
        }

        fn rollback(&self, _store: &dyn MigrationStore) -> anyhow::Result<()> {
            Ok(())
        }

        fn duplicate(&self) -> Box<dyn Changeset> {
            FileChangeset::new(self.path.clone())
        }
    }

    fn empty_changeset(identifier: Identifier) -> Box<dyn Changeset> {
        let raw = identifier.value();
        let path = Path::new(raw);
        if path.exists() {
            warn!("Changeset path {} already exists", raw)
        } else {
            create_dir(path).unwrap();
        }
        FileChangeset::new(path.to_path_buf())
    }

    fn to_changeset(path: PathBuf) -> Box<dyn Changeset> {
        FileChangeset::new(path.clone())
    }

    #[test]
    fn migrations() {
        let identifier = Identifier::from_string("20240620112020_test");
        let store = MigrationStores::in_memory(vec![empty_changeset(identifier.clone())]);
        let path = env::current_dir().expect("Failed to get current directory");
        let changes = ChangesetStores::file_store(path, empty_changeset, to_changeset);
        let mut migrations = Migrations { store, changes };
        migrations.setup().unwrap();

        assert_eq!(migrations.is_applied(&identifier), false);
        migrations.migrate().unwrap();

        assert_eq!(migrations.is_applied(&identifier), true);
    }

    #[test]
    fn rollback() {
        let identifier = Identifier::from_string("20240620112020_test");
        let store = MigrationStores::in_memory(vec![empty_changeset(identifier.clone())]);
        let path = env::current_dir().expect("Failed to get current directory");
        let changes = ChangesetStores::file_store(path, empty_changeset, to_changeset);
        let mut migrations = Migrations { store, changes };
        migrations.setup().unwrap();
        migrations.migrate().unwrap();
        migrations.rollback().unwrap();

        assert_eq!(migrations.is_applied(&identifier), false);
    }
}
