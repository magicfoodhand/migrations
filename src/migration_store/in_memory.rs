use crate::{Changeset, Identifier, MigrationStore};
use std::collections::HashSet;

pub struct InMemoryMigrationStore {
    pub changesets: Vec<Box<dyn Changeset>>,
    pub applied: HashSet<Identifier>,
}

impl InMemoryMigrationStore {
    pub fn create(changesets: Vec<Box<dyn Changeset>>) -> Box<dyn MigrationStore> {
        Box::new(InMemoryMigrationStore {
            changesets,
            applied: HashSet::new(),
        })
    }
}

impl MigrationStore for InMemoryMigrationStore {
    fn is_ready(&self) -> anyhow::Result<bool> {
        Ok(true)
    }

    fn setup(&self) -> anyhow::Result<()> {
        Ok(())
    }

    fn is_applied(&self, identifier: &Identifier) -> bool {
        self.applied.contains(identifier)
    }

    fn apply(&mut self, changeset: &dyn Changeset) -> anyhow::Result<()> {
        changeset.apply(self)?;
        self.applied.insert(changeset.identifier());
        Ok(())
    }

    fn rollback(&mut self, changeset: &dyn Changeset) -> anyhow::Result<()> {
        changeset.rollback(self)?;
        self.applied.remove(&changeset.identifier());
        Ok(())
    }

    fn clone(&self) -> Box<dyn MigrationStore + '_> {
        let mut changesets = Vec::new();
        for changeset in &self.changesets {
            changesets.push(changeset.duplicate());
        }

        let applied = self.applied.clone();

        Box::new(InMemoryMigrationStore {
            changesets,
            applied,
        })
    }
}
