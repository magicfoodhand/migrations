use crate::{Changeset, Identifier};
use std::path::Path;

mod in_memory;
use in_memory::InMemoryMigrationStore;

#[cfg(feature = "libsql")]
use crate::custom::libsql::LibSqlMigrationStore;

pub struct MigrationStores;

impl MigrationStores {
    pub fn in_memory(changesets: Vec<Box<dyn Changeset>>) -> Box<dyn MigrationStore> {
        InMemoryMigrationStore::create(changesets)
    }

    #[cfg(feature = "libsql")]
    pub fn libsql(connection: libsql::Connection) -> Box<dyn MigrationStore> {
        LibSqlMigrationStore::new(connection)
    }
}

pub trait MigrationStore {
    fn is_ready(&self) -> anyhow::Result<bool>;

    fn setup(&self) -> anyhow::Result<()>;

    fn is_applied(&self, identifier: &Identifier) -> bool;

    fn apply(&mut self, changeset: &dyn Changeset) -> anyhow::Result<()>;

    fn rollback(&mut self, changeset: &dyn Changeset) -> anyhow::Result<()>;

    fn clone(&self) -> Box<dyn MigrationStore + '_>;

    fn file_actions(&self) -> Option<&dyn FileActions> {
        None
    }
}

pub trait FileActions {
    fn run(&self, file: &Path) -> anyhow::Result<()>;
}
